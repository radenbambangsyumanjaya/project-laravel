<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function toRegister(){
        return view('form');

    }

    public function toWelcome(Request $ambil){
        $nama = ucfirst(strtolower($ambil["fname"]))." ".ucfirst(strtolower($ambil["lname"]));

        return view('welcome',compact('nama'));

    }
}
