<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Form</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up form</h3>
	<form action='/welcome' method="POST">
		@csrf
		<p>First name:</p>
		<input type="text" name="fname">
		<p>Last name:</p>
		<input type="text" name="lname">
		<p>Gender:</p>
		<input type="radio" name="gender" value="male"> 
		<label for="male">Male</label> <br>
		<input type="radio" name="gender" value="female">
		<label for="female">Female</label> <br>
		<input type="radio" name="gender" value="other">
		<label for="other">Other</label>  <br>
		<p>Nationality:</p>
		<select name="nationality">
			<option value="indonesian">Indonesian</option>
			<option value="singaporean">Singaporean</option>
			<option value="malaysian">Malaysian</option>
			<option value="australian">Australian</option>
		</select>
		<p>Language Spoken:</p>
		<input type="checkbox" name="spoken" value="bahasa">Bahasa Indonesia <br>
		<input type="checkbox" name="spoken" value="english">English <br>
		<input type="checkbox" name="spoken" value="other">Other <br>
		<p>Bio:</p>
		<textarea name="bio" cols="30" rows="10"></textarea> <br>
		<button type="submit">Sign Up</button>


	</form>
</body>
</html>